import { reducer } from 'redux-storage';
import { reducer as ui } from 'react-redux-ui-tools';
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import gameReducer from 'Components/PlayGround/reducers';
import userReducer from 'Components/User/reducers';

export default (history, injectedReducers) => reducer(
  combineReducers({
    router: connectRouter(history),
    ui,
    game: gameReducer,
    user: userReducer,
    ...injectedReducers,
  })
);
