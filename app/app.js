import './global-styles';

import React from 'react';
import { render as ReactDOMRender } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import deepmerge from 'deepmerge';
import { isPlainObject } from 'lodash-es';
import '@babel/polyfill';

import { LOCAL_STORAGE_KEY } from 'Utils/constants';
import updateObject from 'Utils/updateObject';
import configureStore, { history } from 'Store/configureStore';
import initialState from 'Store/initialState';

const store = configureStore(
  deepmerge(
    initialState, updateObject(JSON.parse(window.localStorage.getItem(LOCAL_STORAGE_KEY))), {
      isMergeableObject: isPlainObject,
    }
  )
);

const render = Playground => ReactDOMRender(
  <Provider store={ store }>
    <ConnectedRouter history={ history }>
      <Playground />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app-root')
);

if (module.hot) {
  module.hot.accept('./components/PlayGround/index.js', () => {
    render(require('./components/PlayGround').default); // eslint-disable-line global-require
  });
}

render(require('./components/PlayGround').default);
