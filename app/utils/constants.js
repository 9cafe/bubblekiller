// UI
export const UI_KEY = 'APP_UI';
export const IS_MENU_OPEN = 'app/ui/IS_MENU_OPEN';

// STORAGE
export const LOCAL_STORAGE_KEY = 'bubblekiller';
