const updateObject = (oldObj, ...rest) => Object.assign({}, oldObj, ...rest);

export default updateObject;
