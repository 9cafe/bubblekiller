import { handleActions } from 'redux-actions';
import updateObject from 'Utils/updateObject';
import { updateMaxScore } from './actions';

export const initialState = {
  username: '',
  email: '',
  maxScore: 0,
};

export default handleActions({
  [updateMaxScore]: (state, { payload: { score } }) => updateObject(state, { maxScore: score }),
}, initialState);
