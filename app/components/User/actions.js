import { createAction } from 'redux-actions';
import { MAX_SCORE_UPDATE } from './constants';

export const updateMaxScore = createAction(MAX_SCORE_UPDATE, score => ({ score }));
