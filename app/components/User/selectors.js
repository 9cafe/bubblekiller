import { createSelector } from 'reselect';

const userState = state => state.user;

export const makeSelectUserMaxScore = createSelector(
  userState,
  state => state.maxScore,
);
