import { createAction } from 'redux-actions';
import { END_GAME, UPDATE_SCORE, START_GAME } from './constants';

export const startGame = createAction(START_GAME);
export const endGame = createAction(END_GAME);
export const updateCurrentScore = createAction(UPDATE_SCORE, score => ({ score }));
