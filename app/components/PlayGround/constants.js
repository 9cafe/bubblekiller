export const BASE_CONFIG = {
  color: '#222',
  fps: 60,
};

export const START_GAME = 'app/PlayGround/actions/START_GAME';
export const END_GAME = 'app/PlayGround/actions/END_GAME';
export const UPDATE_SCORE = 'app/PlayGround/actions/UPDATE_SCORE';
