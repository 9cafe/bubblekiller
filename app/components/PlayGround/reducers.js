import { handleActions } from 'redux-actions';
import updateObject from 'Utils/updateObject';
import { endGame, startGame, updateCurrentScore } from './actions';

export const initialState = {
  running: false,
  currentScore: 0,
  minDifficulty: 0,
  maxDifficulty: 1,
  minTimeout: 750,
  startTimeout: 3000,
  missedBubbles: 0,
};

export default handleActions({
  [startGame]: state => updateObject(state, { running: true }),
  [endGame]: state => updateObject(state, { running: false }),
  [updateCurrentScore]: (state, { payload: { score } }) => updateObject(state, { currentScore: score }),
}, initialState);
