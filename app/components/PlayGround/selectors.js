import { createSelector } from 'reselect';

const gameState = state => state.game;

export const makeSelectGameState = createSelector(gameState);

export const makeSelectGameRunning = createSelector(
  gameState,
  state => state.running,
);

export const makeSelectGameScore = createSelector(
  gameState,
  state => state.currentScore,
);
