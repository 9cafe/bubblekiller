import React, { Fragment, PureComponent } from 'react';
import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';
// import injectUi from 'react-redux-ui-tools';
import { bool, func, number } from 'prop-types';
import { hot } from 'react-hot-loader/root';
import * as oCanvas from 'ocanvas';
import Bubble from 'Components/Bubble';
import { BASE_CONFIG } from 'Components/PlayGround/constants';
import updateObject from 'Utils/updateObject';
import AppBar from 'Components/AppBar';
import { makeSelectGameRunning, makeSelectGameScore } from 'Components/PlayGround/selectors';
import { updateMaxScore } from 'Components/User/actions';
import { makeSelectUserMaxScore } from 'Components/User/selectors';
// import { uiConfig } from 'Store/initialState';
import { updateCurrentScore, endGame } from './actions';

const mapStateToProps = state => ({
  gameRunning: makeSelectGameRunning(state),
  currentScore: makeSelectGameScore(state),
  maxScore: makeSelectUserMaxScore(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
  endGame, updateMaxScore, updateCurrentScore,
}, dispatch);

@compose(
  // injectUi(uiConfig),
  connect(mapStateToProps, mapDispatchToProps)
)
class Playground extends PureComponent {
  static propTypes = {
    // ui: object,
    updateMaxScore: func,
    endGame: func,
    updateCurrentScore: func,
    gameRunning: bool,
    currentScore: number,
    maxScore: number,
  };

  defaultState = {
    minDifficulty: 0,
    maxDifficulty: 1,
    minTimeout: 750,
    startTimeout: 3000,
    missedBubbles: 0,
  };

  constructor(props) {
    super(props);
    this.state = updateObject(this.defaultState);
  }

  componentDidMount() {
    this.canvas = oCanvas.create({
      canvas: '#canvas',
      background: BASE_CONFIG.color,
      fps: BASE_CONFIG.fps,
    });
    this.canvas.width = document.documentElement.clientWidth;
    this.canvas.height = document.documentElement.clientHeight;
  }

  componentDidUpdate(prevProps) {
    const { gameRunning } = this.props;
    const { gameRunning: wasGameRunning } = prevProps;
    if (!wasGameRunning && gameRunning) {
      this.startGame();
    }
    if (wasGameRunning && !gameRunning) {
      this.endGame();
    }
  }

  componentWillUnmount() {
    this.canvas.destroy();
  }

  newBubbleTimeout = () => {
    const { minTimeout, startTimeout, minDifficulty, maxDifficulty } = this.state;
    return Math.max(
      minTimeout,
      startTimeout - startTimeout * Math.min(minDifficulty, maxDifficulty)
    );
  };

  randomPos = radius => ({
    x: Math.max(
      radius,
      Math.random() * document.documentElement.clientWidth - radius
    ),
    y: Math.max(
      radius + 60,
      Math.random() * document.documentElement.clientHeight - radius
    ),
  });

  createBubble() {
    const radius = Bubble.getRandomRadius();
    const { x, y } = this.randomPos(radius);
    const newBubble = new Bubble({
      timeout: this.newBubbleTimeout(),
      radius,
      x,
      y,
      playground: this,
    });
    newBubble.render();
  }

  deleteBubble(actionType, bubbleInstance) {
    const { currentScore, endGame, gameRunning, updateCurrentScore } = this.props;
    const { missedBubbles, minDifficulty } = this.state;
    console.warn(`Deleting bubble because ${actionType}`);
    if (actionType === 'userDeleted') {
      updateCurrentScore(currentScore + 1);
    } else if (actionType === 'timedOut') {
      this.setState({ missedBubbles: missedBubbles + 1 });
    }

    if (missedBubbles >= 10) {
      endGame();
    } else {
      this.canvas.removeChild(bubbleInstance.bubble);
      bubbleInstance.destroy();
      this.setState({ minDifficulty: minDifficulty + 0.025 });
      if (gameRunning) {
        this.createBubble();
      }
    }
  }

  resetConfig() {
    const { updateMaxScore, maxScore, updateCurrentScore, currentScore } = this.props;
    if (currentScore > maxScore) {
      updateMaxScore(currentScore);
    }
    updateCurrentScore(0);
    this.setState(this.defaultState);
  }

  startGame() {
    console.log('%c___GAME STARTED___', 'color: green');
    this.createBubble();
  }

  endGame() {
    const { currentScore } = this.props;
    console.log('%c___GAME ENDED___', 'color: red');
    alert(`Your score is ${currentScore}`); // TODO: replace with modal
    this.resetConfig();
    this.canvas.reset();
  }

  render() {
    return (
      <Fragment>
        <AppBar />
        <canvas id="canvas" />
      </Fragment>
    );
  }
}

export default hot(Playground);
