import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { func, bool, number } from 'prop-types';
import { css } from 'emotion';
import { endGame, startGame } from 'Components/PlayGround/actions';
import { makeSelectUserMaxScore } from 'Components/User/selectors';
import { makeSelectGameRunning, makeSelectGameScore } from 'Components/PlayGround/selectors';

const styles = {
  root: css({
    display: 'flex',
    justifyContent: 'space-between',
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100vw',
    height: 50,
    backgroundColor: '#efc0c0',
    zIndex: 1,
  }),
  score: css({
    p: {
      margin: 0,
      fontSize: 12,
    },
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  }),
  button: css({
    cursor: 'pointer',
    padding: '0 15px',
  })
};

const mapStateToProps = state => ({
  isGameRunning: makeSelectGameRunning(state),
  userScore: makeSelectGameScore(state),
  maxScore: makeSelectUserMaxScore(state),
});

const mapDispatchToProps = dispatch => ({
  toggle: start => dispatch(start ? startGame() : endGame()),
});

export default
@connect(mapStateToProps, mapDispatchToProps)
class AppBar extends PureComponent {
  static propTypes = {
    isGameRunning: bool,
    toggle: func,
    maxScore: number,
    userScore: number,
  };

  render() {
    const { isGameRunning, userScore, maxScore, toggle } = this.props;
    return (
      <header className={ styles.root }>
        <button type="button" className={ styles.button } onClick={ () => toggle(true) }>
          Start
        </button>
        { isGameRunning && (
          <div className={ styles.score }>
            <p>Current: { userScore }</p>
            { maxScore ? (<p>(Best: { maxScore })</p>) : null }
          </div>
        ) }
        {
          (!isGameRunning && maxScore)
            ? <p>Best: { maxScore }</p>
            : null
        }
        <button type="button" className={ styles.button } onClick={ () => toggle(false) }>
          End
        </button>
      </header>
    );
  }
}
