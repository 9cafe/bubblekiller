import { COLORS } from './constants';

export default class Bubble {
  constructor(props) {
    this.props = props;
    this.handleClick = this.handleClick.bind(this);
    this.fill = COLORS[Math.round(Math.random() * (COLORS.length - 1))];
    this.timeout = setTimeout(
      () => this.animateOut.call(this, 'timedOut'),
      props.timeout
    );
  }

  bubble = null;

  static getRandomRadius() {
    return Math.max(30, Math.random() * 50);
  }

  handleClick(e) {
    e.preventDefault();
    this.animateOut('userDeleted');
  }

  animateOut(actionType) {
    const { playground } = this.props;
    clearTimeout(this.timeout);
    this
      .bubble
      .unbind('click tap', this.handleClick)
      .stop()
      .animate({
        radius: 0,
      }, {
        duration: 350,
        easing: 'ease-out-cubic',
        callback: () => {
          playground.deleteBubble(actionType, this);
        },
      });
  }

  destroy() {
    clearTimeout(this.timeout);
    this.bubble.unbind('click tap', this.handleClick);
    this.bubble.remove();
    this.bubble = null;
  }

  render() {
    const { playground: { canvas }, radius, x, y } = this.props;

    this.bubble = canvas.display.ellipse({ x, y, radius: 0, fill: this.fill });
    this.bubble.bind('click tap', this.handleClick);
    this.bubble.animate({
      radius,
    }, {
      duration: 200,
      easing: 'ease-in-cubic',
    });
    canvas.addChild(this.bubble);
    console.info(
      `Created %c${this.fill.toUpperCase()} %c bubble with radius ${radius} created at [${x}, ${y}]`,
      `color: ${this.fill}`,
      'color: black',
    );
  }
}
