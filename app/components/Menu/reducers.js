import handleActions from 'redux-actions';
import { IS_MENU_OPEN } from 'App/utils/constants';
import { menuOpen, menuClose } from './actions';

export default handleActions({
  [menuOpen]: () => ({ [IS_MENU_OPEN]: true }),
  [menuClose]: () => ({ [IS_MENU_OPEN]: true }),
}, { });