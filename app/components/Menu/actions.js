import { createAction } from 'redux-actions';
import { MENU_OPEN, MENU_CLOSE } from 'App/components/Menu/constants';

export const menuOpen = createAction(MENU_OPEN);
export const menuClose = createAction(MENU_CLOSE);
