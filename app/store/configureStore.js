import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { createMiddleware, createLoader } from 'redux-storage';
import createEngine from 'redux-storage-engine-localstorage';
import debounce from 'redux-storage-decorator-debounce';
import filter from 'redux-storage-decorator-filter';
import { composeWithDevTools } from 'redux-devtools-extension/logOnlyInProduction';
import { createBrowserHistory } from 'history';
import createRootReducer from 'Reducers';
import { LOCAL_STORAGE_KEY } from 'Utils/constants';
import { MAX_SCORE_UPDATE } from 'Components/User/constants';

const storageEngine = debounce(
  filter(
    createEngine(LOCAL_STORAGE_KEY),
    [ // whitelist
      'ui',
      'user',
    ],
  ),
  1000 // 1s debounce.
);

const localStorageMiddleware = createMiddleware(storageEngine, [MAX_SCORE_UPDATE]);
export const history = createBrowserHistory();
/**
 * This will create the enhanced Redux Store
 * @param initialState {Map|Object}
 * @returns {Object} - Store<Reducer, State, Enhancer>
 */
export default function configureStore(initialState) {
  /** Redux middleware collection
   * 1. sagaMiddleware: Makes redux-sagas work
   * 2. storageMiddleware: Syncs store with local storage
   * 3. routerMiddleware: Syncs state with window.location
   */
  const middlewares = [
    localStorageMiddleware,
    routerMiddleware(history),
  ];

  const enhancers = [
    applyMiddleware(...middlewares),
  ];

  const composeEnhancers = composeWithDevTools({
    shouldHotReload: false,
  });

  /**
   * App store contains:
   * a. App reducers (routerReducer, languageReducer, authReducer etc)
   * b. Initial application state
   * c. Other store enhancers: (ReduxDevtoolsExtension, Redux-Saga, routerMiddleware)
   */
  const store = createStore(
    createRootReducer(history),
    initialState,
    composeEnhancers(...enhancers),
  );

  // Load the Store with data from localStorage
  createLoader(storageEngine)(store);

  store.injectedReducers = {}; // Reducer registry

  // Make reducers hot reloadable, see http://mxs.is/googmo
  /* istanbul ignore next */
  if (module.hot) {
    module.hot.accept('../reducers', () => {
      store.replaceReducer((createRootReducer(history, store.injectedReducers)));
    });
  }

  return store;
}
