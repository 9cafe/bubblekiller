import { IS_MENU_OPEN, UI_KEY } from 'Utils/constants';
import { initialState as initialUserState } from 'Components/User/reducers';
import { initialState as initialGameState } from 'Components/PlayGround/reducers';

const uiDefaultState = {
  [IS_MENU_OPEN]: false,
};

export const uiConfig = {
  key: UI_KEY,
  persist: true,
  state: uiDefaultState,
};

const initialState = {
  user: initialUserState,
  game: initialGameState,
};

export default initialState;
