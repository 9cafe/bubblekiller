const { resolve, join, basename } = require('path');
const fs = require('fs');
const glob = require('glob');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const AddAssetHtmlPlugin = require('add-asset-html-webpack-plugin');
const appRoot = require('app-root-path');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');
const { dllPlugin } = appRoot.require('package.json');
const baseConfig = require('./webpack/webpack.base');

const plugins = [
  new HtmlWebpackPlugin({
    template: resolve('index.html'),
    showErrors: true,
    cache: false,
    inject: true,
    minify: {
      collapseWhitespace: true,
      collapseInlineTagWhitespace: true,
      removeComments: true,
      removeRedundantAttributes: true,
    },
  }),
  new DuplicatePackageCheckerPlugin(),
];

if (dllPlugin) {
  glob.sync(appRoot.resolve(`/${dllPlugin.path}/*.dll.js`)).forEach(dllPath => {
    plugins.push(
      new AddAssetHtmlPlugin({
        filepath: dllPath,
        includeSourcemap: false,
      })
    );
  });
}

/**
 * Select which plugins to use to optimize the bundle's handling of
 * third party dependencies.
 *
 * If there is a dllPlugin key on the project's package.json, the
 * Webpack DLL Plugin will be used.  Otherwise the CommonsChunkPlugin
 * will be used.
 *
 */
function dependencyHandlers() {
  // Don't do anything during the DLL Build step
  if (process.env.BUILDING_DLL) {
    return [];
  }

  const dllPath = appRoot.resolve(dllPlugin.path || 'node_modules/react-boilerplate-dlls');

  /**
   * If DLLs aren't explicitly defined, we assume all production dependencies listed in package.json
   * Reminder: You need to exclude any server side dependencies by listing them in dllConfig.exclude
   */
  if (!dllPlugin.dlls) {
    const manifestPath = resolve(dllPath, 'reactBoilerplateDeps.json');

    if (!fs.existsSync(manifestPath)) {
      console.error('The DLL manifest is missing. Please run `npm run build:dll`');
      process.exit(0);
    }

    return [
      new webpack.DllReferencePlugin({
        context: appRoot.toString(),
        manifest: require(manifestPath), // eslint-disable-line global-require
      }),
    ];
  }

  // If DLLs are explicitly defined, we automatically create a DLLReferencePlugin for each of them.
  const dllManifests = Object.keys(dllPlugin.dlls).map(name => join(dllPath, `/${name}.json`));

  return dllManifests.map(manifestPath => {
    if (!fs.existsSync(dllPath)) {
      if (!fs.existsSync(manifestPath)) {
        console.error(`The following Webpack DLL manifest is missing: ${basename(manifestPath)}`);
        console.error(`Expected to find it in ${dllPath}`);
        console.error('Please run: npm run build:dll');

        process.exit(0);
      }
    }

    return new webpack.DllReferencePlugin({
      context: appRoot.toString(),
      manifest: require(manifestPath), // eslint-disable-line global-require
    });
  });
}

const config = baseConfig({
  entry: appRoot.resolve('app/app.js'),
  output: {
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
  },
  module: {
    rules: [],
  },
  // Add development plugins
  plugins: dependencyHandlers().concat(plugins), // eslint-disable-line no-use-before-define
  devtool: 'eval-source-map',
  watch: true,
  mode: 'development',
  devServer: {
    contentBase: appRoot.resolve('dist'),
    compress: true,
    hot: true,
    host: '0.0.0.0',
    port: 3000,
    historyApiFallback: true,
    clientLogLevel: 'info',
  },
  performance: {
    hints: false,
    maxAssetSize: 200000,
    maxEntrypointSize: 400000,
    assetFilter: assetFilename => assetFilename.endsWith('.css') || assetFilename.endsWith('.js'),
  },
});

module.exports = config;
