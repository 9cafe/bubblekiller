/**
 * COMMON WEBPACK CONFIGURATION
 */

const webpack = require('webpack');
const merge = require('lodash/fp/merge');
const appRoot = require('app-root-path');

process.traceDeprecation = true;
// process.noDeprecation = true;

module.exports = options => ({
  ...options,
  entry: options.entry,
  output: Object.assign({
    path: appRoot.resolve('/dist'),
    publicPath: '/',
  }, options.output),
  optimization: Object.assign({
    namedModules: true,
  }, options.optimization),
  module: {
    rules: Array.prototype.concat.call([
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
      },
    ], (() => {
      if (options && options.module && options.module.rules) {
        return options.module.rules;
      }
      return [];
    })()),
  },
  plugins: [
    new webpack.ProvidePlugin({
      React: 'react',
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV),
      },
    }),
  ].concat(options.plugins),
  resolve: merge({
    modules: [
      appRoot.resolve('node_modules'),
      appRoot.resolve('app'),
    ],
    extensions: ['.js'],
    mainFields: ['browser', 'jsnext:main', 'main'],
    symlinks: false,
    alias: {
      Components: appRoot.resolve('app/components'),
      Containers: appRoot.resolve('app/containers'),
      Reducers: appRoot.resolve('app/reducers'),
      Utils: appRoot.resolve('app/utils'),
      Store: appRoot.resolve('app/store'),
      Styles: appRoot.resolve('app/styles'),
      Selectors: appRoot.resolve('app/selectors'),
      Actions: appRoot.resolve('app/actions'),
    },
  }, options.resolve),
  devtool: options.devtool || 'source-map',
  mode: options.mode || 'production',
  watch: options.watch || false,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  performance: options.performance || {},
});
