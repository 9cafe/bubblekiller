const appRoot = require('app-root-path');

module.exports = {
  resolve: {
    alias: {
      App: appRoot.resolve('app'),
      Components: appRoot.resolve('app/components'),
      Reducers: appRoot.resolve('app/reducers'),
      Utils: appRoot.resolve('app/utils'),
      Store: appRoot.resolve('app/store'),
      Styles: appRoot.resolve('app/styles'),
      Selectors: appRoot.resolve('app/selectors'),
      Actions: appRoot.resolve('app/actions'),
    },
  },
};
