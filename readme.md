# Bubble Killer

A small game where you have to click/touch in time each bubble that spawns on your screen before it expire.
You never win, but you have a best score. You lose when you miss 5 bubbles (from clicking/touching).
Difficulty increases with your score.

### 1. Setup

1. [Clone the project locally](https://bitbucket.org/9cafe/bubblekiller/src/master/)
2. From the root of the project, open up a terminal and either run `yarn install` (if you have
**yarn** installed) or `npm install` to add the required external project dependencies.

### 2. Running

From the root of the project, open up a terminal and run `npm start` to run the code with a local server


### Development diagram/schema
https://whimsical.co/ULPGArRmVmQ4AQTkeAF5Dn


**TODO:**

- Implement better GAME UI;
- Implement smoother animations;
- Implement countdown;
- Implement better feedback response when you interact with bubbles;
- Create a leaderboard;
- Implement react-native to push it on google store/iOS;
- ~~Migrate to react and redux;~~
- ~~Implement webpack;~~
- ~~Find better colors;~~
- ~~Implement Start/Stop;~~
- ~~Implement userScore;~~
- ~~Initial code release;~~

**Bubble Killer** is currently extended with the following libraries:

- [oCanvas](https://reactjs.org/) - Development with HTML5 Canvas easier.
- [ReactJS](https://reactjs.org/) - A JavaScript library for building user interfaces.
- [Redux](https://redux.js.org) - A predictable state container for JavaScript apps.
